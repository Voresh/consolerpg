using System;

namespace RPG
{
	public class Unit : MenuObject
	{
		public Unit (string n, string test)
		{
			this.name = n;
			this.interactionAction = new DisplayText(test);
		}
	}
}

