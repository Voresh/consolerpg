using System;

namespace RPG
{
	public sealed class Path : MenuObject
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="RPG.Path"/> class.
		/// </summary>
		/// <param name="locToMove">Location to move and element name</param>
		public Path (string locToMove)
		{
			this.name = locToMove;
			this.interactionAction = new MoveToLocation(locToMove);
		}
	}
}

