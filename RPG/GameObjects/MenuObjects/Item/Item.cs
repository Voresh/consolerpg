using System;

namespace RPG
{
	public class Item : MenuObject
	{
		public Item (string n, string test)
		{
			this.name = n;
			this.interactionAction = new DisplayText(test);
		}
	}
}

