using System;
using System.Collections.Generic;

namespace RPG
{
	public sealed class Location : GameObject
	{
		private List<Path> paths = new List<Path>();
		private List<Item> items = new List<Item>();
		private List<Unit> units = new List<Unit>();

		public Location(string n) 
		{
			name = n;
		}

		public string GetName()
		{
			return name;
		}

		public void AddPath(Path p)
		{
			paths.Add(p);
		}

		public List<Path> GetPaths()
		{
			return paths;
		}

		public List<Item> GetItems()
		{
			return items;
		}

		public List<Unit> GetUnits()
		{
			return units;
		}
	}
}

