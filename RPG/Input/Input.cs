using System;

//переделать с учетом шаблона команда

namespace RPG
{
	public class Input
	{
		public void UpdateInput()
		{
			ConsoleKeyInfo c = Console.ReadKey ();
			
			if (c.Key == ConsoleKey.PageDown)
			{
				MainClass._GUI.GetCurrentMenu().MoveToNextElement();
			}
			else if (c.Key == ConsoleKey.PageUp)
			{
				MainClass._GUI.GetCurrentMenu().MoveToPriviousElement();
			}
			else if (c.Key == ConsoleKey.Q)
			{
				MainClass._GUI.GetCurrentMenu().InteractWithSelectedElement();
			}
		}
	}
}

