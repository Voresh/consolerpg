using System;

namespace RPG
{
	public abstract class MenuObject : GameObject
	{
		protected Command interactionAction;

		public string GetName()
		{
			return name;
		}

		public void Interact()
		{
			interactionAction.Execute();
		}
	}
}

