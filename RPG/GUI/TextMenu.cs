using System;
using System.Collections.Generic;

namespace RPG
{
	public class TextMenu
	{ 
		private string title = "";
		private List<MenuElement> elements = new List<MenuElement> ();
		private int selectedElement        = 0;

		private TextMenu priviousMenu;
		private TextMenu nextMenu;

		public TextMenu (string t) 
		{
			this.title = t + ":";
		}

		public TextMenu (string t, TextMenu privious, TextMenu next)
		{
			this.title = t + ":";
			this.priviousMenu = privious;
			this.nextMenu     = next;
		}

		#region public methods
		public void SetPriviousMenu(TextMenu privious)
		{
			priviousMenu = privious;
		}

		public void SetNextMenu(TextMenu next)
		{
			nextMenu     = next;
		}

		public string GetTitle()
		{
			return title;
		}

		public string GetElementText(int id)
		{
			return elements[id].GetText();
		}

		public string GetSelectedElementText()
		{
			return elements[selectedElement].GetText();
		}

		public void AddElementToMenu(MenuElement e)
		{
			elements.Add (e);
		}

		public void RemoveElementFromMenu(MenuElement e)
		{
			elements.Remove (e);
		}

		public int GetElementsCount()
		{
			return elements.Count;
		}

		public int GetSelectedElement()
		{
			return selectedElement;
		}

		public void ClearMenu()
		{
			elements.Clear ();
		}
		
		public void MoveToPriviousElement()
		{
			if (selectedElement - 1 >= 0)
			{
				selectedElement--;
			}
			else
			{
				if (priviousMenu != null)
				{
					this.LostFocus();
					priviousMenu.SetFocus();
				}
			}
		}

		public void MoveToNextElement()
		{
			if (selectedElement + 1 < elements.Count)
			{
				selectedElement++;
			}
			else
			{
				if (nextMenu != null)
				{
					this.LostFocus();
					nextMenu.SetFocus();
				}
			}
		}

		public void InteractWithSelectedElement()
		{
			elements [selectedElement].Interact ();
		}

		public void SetFocus()
		{
			MainClass._GUI.SetCurrentMenu (this);
		}

		public void LostFocus()
		{
			ResetSelection ();
		}

		public void ResetSelection()
		{
			selectedElement = 0;
		}
		#endregion
	}
}

