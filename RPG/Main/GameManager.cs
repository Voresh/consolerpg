using System;
using System.Collections.Generic;

namespace RPG
{
	public class GameManager
	{
		private Location currentLocation = null;
		private Dictionary<string, Location> locations = new Dictionary<string, Location>();

		#region public methods
		public void Start()
		{
			createLocations();
			addPaths();
			SetCurrentLocation ("Main hall");
		}
		
		public void Update()
		{	
			if (currentLocation != null)
			{
				MainClass._GUI.DisplayLocationInfo(currentLocation);
				MainClass._Input.UpdateInput();
			}
		}

		public void SetLocationToSelected()
		{
			currentLocation = locations[MainClass._GUI.GetCurrentMenu().GetSelectedElementText()];
		}

		public void SetCurrentLocation(string name)
		{
			currentLocation = locations[name];
			MainClass._GUI.UpdateMenuElements(currentLocation);
		}

		public Location GetCurrentLocation()
		{
			return currentLocation;
		}
		#endregion

		private void createLocations()
		{
			addLocation("Main hall");
			addLocation("Forest");
			addLocation("Fields");
			addLocation("Lake");
		}

		private void addLocation(string name)
		{
			locations.Add(name, new Location(name));
		}

		private void addPaths()
		{
			locations["Main hall"].AddPath(new Path("Fields"));
			locations["Main hall"].AddPath(new Path("Forest"));
			locations["Main hall"].AddPath(new Path("Lake"));
		}
	}
}

