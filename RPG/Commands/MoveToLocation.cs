using System;

namespace RPG
{
	public class MoveToLocation : Command
	{
		private string locationName;

		public MoveToLocation (string locname)
		{
			this.locationName = locname;
		}

		public override void Execute ()
		{
			MainClass._GameManager.SetCurrentLocation (locationName);
		}
	}
}

