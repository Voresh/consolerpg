using System;
using System.Collections.Generic;

namespace RPG
{
	public class GUI
	{
		private TextMenu pathsMenu;
		private TextMenu unitMenu;
		private TextMenu itemMenu;

		private TextMenu currentMenu;

		public string DebugLog = "";

		public GUI()
		{
			pathsMenu = new TextMenu ("Paths");
			currentMenu  = pathsMenu;

			unitMenu = new TextMenu ("Units");
			itemMenu = new TextMenu ("Items");

			pathsMenu.SetNextMenu (unitMenu);
			unitMenu.SetPriviousMenu (pathsMenu);
			unitMenu.SetNextMenu (itemMenu);
			itemMenu.SetPriviousMenu (unitMenu);
		}

		public void DisplayLocationInfo(Location curloc)
		{
			Console.Clear ();
			Console.WriteLine("Location: " + curloc.GetName()); 

			displaySeparator ();
			displayTextMenu (pathsMenu);
			displaySeparator ();
			displayTextMenu (unitMenu);
			displaySeparator ();
			displayTextMenu (itemMenu);

			Console.WriteLine("\n" + "Debug: " + DebugLog); 
		}

		public TextMenu GetCurrentMenu()
		{
			return currentMenu;
		}
		
		public void SetCurrentMenu(TextMenu menu)
		{
			currentMenu = menu;
		}

		public void UpdateMenuElements(Location currLoc)
		{
			associateObjects(pathsMenu, unitMenu, itemMenu, currLoc.GetPaths(), currLoc.GetUnits(), currLoc.GetItems());
		}

		private void associateObjects(TextMenu pm,TextMenu um,TextMenu im, List<Path> p,List<Unit> u, List<Item> i)//КАРАУЛ
		{
			pm.ClearMenu ();
			pm.ResetSelection();

			foreach (MenuObject obj in p)
			{
				pm.AddElementToMenu(new MenuElement(obj));
			}

			um.ClearMenu ();
			um.ResetSelection();
			
			foreach (MenuObject obj in i)
			{
				um.AddElementToMenu(new MenuElement(obj));
			}

			im.ClearMenu ();
			im.ResetSelection();
			
			foreach (MenuObject obj in u)
			{
				im.AddElementToMenu(new MenuElement(obj));
			}
		}

		private void displayTextMenu(TextMenu menu)
		{
			Console.WriteLine(menu.GetTitle ());
			displaySeparator ();

			if (menu == currentMenu)
			{
				displayFocusedMenu(menu);
			}
			else
			{
				displayUnfocusedMenu(menu);
			}
		}

		private void displayFocusedMenu(TextMenu menu)
		{
			for (int iterator = 0; iterator < menu.GetElementsCount(); iterator++)
			{
				if (iterator == menu.GetSelectedElement())
				{
					Console.WriteLine(">" + menu.GetElementText(iterator));
				}
				else
				{
					Console.WriteLine (menu.GetElementText(iterator));
				}
			}
		}

		private void displayUnfocusedMenu(TextMenu menu)
		{
			for (int iterator = 0; iterator < menu.GetElementsCount(); iterator++)
			{
				Console.WriteLine (menu.GetElementText(iterator));
			}
		}
		
		private void displaySeparator()
		{
			Console.WriteLine("------------------------------");
		}
	}
}

