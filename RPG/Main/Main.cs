using System;

namespace RPG
{
	class MainClass
	{
		private static bool inGame = true;

		public static GameManager _GameManager;
		public static GUI         _GUI;
		public static Input       _Input;

		public static void Main (string[] args)
		{
			_GameManager = new GameManager ();
			_GUI         = new GUI();
			_Input       = new Input();

			_GameManager.Start ();

			while (inGame)
			{
				_GameManager.Update ();
			}
		}

		public void ExitGame()
		{
			inGame = false;
		}
	}
}
