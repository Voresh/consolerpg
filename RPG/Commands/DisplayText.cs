using System;
/// <summary>
/// test!!!
/// </summary>
namespace RPG
{
	public class DisplayText : Command
	{
		private string text;

		public DisplayText (string txt)
		{ 
			this.text = txt;
		}

		public override void Execute ()
		{
			MainClass._GUI.DebugLog = text;
		}
	}
}

