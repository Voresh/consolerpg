using System;

namespace RPG
{
	public abstract class Command
	{
		public abstract void Execute ();
	}
}

