using System;

namespace RPG
{
	public class MenuElement
	{
		private MenuObject associatedObject;

		public MenuElement(MenuObject obj)
		{
			this.associatedObject = obj;
		}

		#region public methods

		public string GetText()
		{
			return associatedObject.GetName();
		}

		public void Interact()
		{
			associatedObject.Interact();
		}

		#endregion
	}
}

